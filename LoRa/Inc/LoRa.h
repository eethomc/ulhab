/*******************************************************************************
 * File:                    LoRa.h
 * Project:                 ULHAB LoRa
 * Date:                    14/01/2019
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             LoRa Interfacing
 ******************************************************************************/

#ifndef LORA_H_
#define LORA_H_

/*******************************************************************************
 * Module includes
 ******************************************************************************/
#include "main.h"
#include "SX1278.h"
#include "spi.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

/*******************************************************************************
 * System Wide Module definitions
 ******************************************************************************/
#define LORA_FREQ 			SX1278_433MHZ
#define LORA_POWER			SX1278_POWER_20DBM
#define LORA_RATE			SX1278_LORA_SF_7
#define LORA_BANDWIDTH		SX1278_LORA_BW_125KHZ
#define LORA_PACKET_LENGTH 	100

#define LORA_TIMEOUT 		1000

#define LORA_SOURCE 		1
#define LORA_DEST			1
#define LORA_PACKET_EXCESS	3 //number of extra bytes, excluding the payload and header

/*******************************************************************************
 * Module Specific Macros
 ******************************************************************************/

/*******************************************************************************
 * Module specific types
 ******************************************************************************/
typedef enum {
	NO_PACKET_TYPE 	= (uint8_t)0,
	GPS_PACKET		= (uint8_t)1,
	TEMP_PRES_PACKET,
} LoRa_Packet_et;

typedef void *LoRa_Packet_Payload_t;

typedef struct {
	float latitude;
	float longitude;
	float altitude;
	float speed;
	uint32_t time;
} GPS_Packet_t;

typedef struct {
	float temperature;
	float pressure;
} Temp_Pres_Packet_t;

typedef struct {
	uint8_t src_id;
	uint8_t dst_id;
	uint8_t id;
} LoRa_Packet_Header_t;

typedef struct {
	LoRa_Packet_et type;
	uint8_t payload_len;
	LoRa_Packet_Payload_t payload;
} LoRa_Packet_t;




/*******************************************************************************
 * Module Globals
 ******************************************************************************/
SX1278_hw_t SX1278_hw;
SX1278_t SX1278;


LoRa_Packet_Header_t header;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void LoRa_Begin();
bool LoRa_SendPacket(LoRa_Packet_t *packet);

#endif
