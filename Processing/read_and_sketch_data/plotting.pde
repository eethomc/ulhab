int lastSecond = 0;

int graphLabelSpacing = 10;

int altCol[] =   {0, 0, 255};
int tempCol[] =  {10,180,10};
int presCol[] =  {255, 0, 0};
int speedCol[] = {255, 0, 180};
int radCol[] =   {0, 180, 180};
 
void DrawPlot()
{
  if(second() != lastSecond)
  {
    lastSecond = second();
    int offset = second();
    
    stroke(255,255,255);
    fill(255,255,255);
    rect(0, alignBox_y-25,boxSize_x+ alignBox_x + 20,height-alignBox_y+25);
     
    fill(0,0,0);
    text(String.format("%02d:%02d:%02d", hour(), minute(), second()), alignBox_x, alignBox_y+boxSize_y+50);
    
    stroke(0,0,0);
    fill(245,245,245);
    rect(alignBox_x,alignBox_y,boxSize_x,boxSize_y);
    
    stroke(150,150,150);
    
    textSize(8);
    textAlign(RIGHT);
    fill(altCol[0], altCol[1], altCol[2]);
    text(String.format("%.02f", alt_max), alignBox_x-5, alignBox_y-1*graphLabelSpacing);
    text(String.format("%.02f", alt_min), alignBox_x-5, alignBox_y+boxSize_y-3*graphLabelSpacing);
    fill(tempCol[0], tempCol[1], tempCol[2]);
    text(String.format("%.02f", temp_max), alignBox_x-5, alignBox_y);
    text(String.format("%.02f", temp_min), alignBox_x-5, alignBox_y+boxSize_y-2*graphLabelSpacing);
    fill(presCol[0], presCol[1], presCol[2]);
    text(String.format("%.02f", pres_max), alignBox_x-5, alignBox_y+1*graphLabelSpacing);
    text(String.format("%.02f", pres_min), alignBox_x-5, alignBox_y+boxSize_y-1*graphLabelSpacing);
    fill(speedCol[0], speedCol[1], speedCol[2]);
    text(String.format("%.02f", speed_max), alignBox_x-5, alignBox_y+2*graphLabelSpacing);
    text(String.format("%.02f", speed_min), alignBox_x-5, alignBox_y+boxSize_y);
    fill(radCol[0], radCol[1], radCol[2]);
    text(String.format("%.02f", rad_count_max), alignBox_x-5, alignBox_y+3*graphLabelSpacing);
    text(String.format("%.02f", rad_count_min), alignBox_x-5, alignBox_y+boxSize_y+1*graphLabelSpacing);
    
    for(int i = 1; i < 4; i++)
    {
      int y = alignBox_y + boxSize_y / 4 * i;
      line(alignBox_x, y, alignBox_x + boxSize_x, y);
      
      fill(altCol[0], altCol[1], altCol[2]);
      text(String.format("%.02f", alt_max-i*(alt_max-alt_min)/4), alignBox_x-5, y-1.5*graphLabelSpacing);
      fill(tempCol[0], tempCol[1], tempCol[2]);
      text(String.format("%.02f", temp_max-i*(temp_max-temp_min)/4), alignBox_x-5, y-0.5*graphLabelSpacing);
      fill(presCol[0], presCol[1], presCol[2]);
      text(String.format("%.02f", pres_max-i*(pres_max-pres_min)/4), alignBox_x-5, y+0.5*graphLabelSpacing);
      fill(speedCol[0], speedCol[1], speedCol[2]);
      text(String.format("%.02f", speed_max-i*(speed_max-speed_min)/4), alignBox_x-5, y+1.5*graphLabelSpacing);
      fill(radCol[0], radCol[1], radCol[2]);
      text(String.format("%.02f", rad_count_max-i*(rad_count_max-rad_count_min)/4), alignBox_x-5, y+2.5*graphLabelSpacing);
    }
    
    for(int i = 0; i < boxSize_x; i++)
    {
      if((i-offset) % 60 == 0)
      {
        int x = alignBox_x + (boxSize_x - i);
        line(x, alignBox_y, x, alignBox_y + boxSize_y);
        
        int now = 3600*hour()+60*minute()+second();
        int then = now - (i);
        int h = (then / 3600);
        int m = (then % 3600) / 60;
        int s = (then % 3600) % 60;
        
        fill(0,0,0);
        textAlign(CENTER);
        textSize(8);
        text(String.format("%02d:%02d:%02d",h,m,s),x, alignBox_y+boxSize_y+15);
        textAlign(LEFT);
      }
    }
    
    for(int i = 0; i < table.lastRowIndex(); i++)
    {
      TableRow row = table.getRow(i);
      int currentTime = hour()*3600+minute()*60+second();
      int x = currentTime - row.getInt("time");
      
      if(x > boxSize_x)
        continue;
        
      strokeWeight(4);
      
      float y_f = row.getFloat("altitude");
      y_f = map(y_f, alt_max, alt_min, 0, boxSize_y-2);
      int y = int(y_f);
      stroke(0, 0, 255);
      point(alignBox_x + (boxSize_x - x), alignBox_y + y + 1);
      
      y_f = row.getFloat("temperature");
      y_f = map(y_f, temp_max, temp_min, 0, boxSize_y-2);
      y = int(y_f);
      stroke(10,180,10);
      point(alignBox_x + (boxSize_x - x), alignBox_y + y + 1);
      
      y_f = row.getFloat("pressure");
      y_f = map(y_f, pres_max, pres_min, 0, boxSize_y-2);
      y = int(y_f);
      stroke(255, 0, 0);
      point(alignBox_x + (boxSize_x - x), alignBox_y + y + 1);
      
      y_f = row.getFloat("speed");
      y_f = map(y_f, speed_max, speed_min, 0, boxSize_y-2);
      y = int(y_f);
      stroke(255, 0, 180);
      point(alignBox_x + (boxSize_x - x), alignBox_y + y + 1);
      
      y_f = row.getFloat("radiation counts");
      y_f = map(y_f, rad_count_max, rad_count_min, 0, boxSize_y-2);
      y = int(y_f);
      stroke(0, 180, 180);
      point(alignBox_x + (boxSize_x - x), alignBox_y + y + 1);
      
      strokeWeight(1);
    }
  }
}
