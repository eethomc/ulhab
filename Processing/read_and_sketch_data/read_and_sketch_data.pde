// Pro_Graph2.pde
/*
 Used in the Youtube video "Arduino and Processing ( Graph Example )"
 Based in the Tom Igoe example.
 Mofified by Arduining 17 Mar 2012:
  -A wider line was used. strokeWeight(4);
  -Continuous line instead of vertical lines.
  -Bigger Window size 600x400.
-------------------------------------------------------------------------------
This program takes ASCII-encoded strings
from the serial port at 9600 baud and graphs them. It expects values in the
range 0 to 1023, followed by a newline, or newline and carriage return

Created 20 Apr 2005
Updated 18 Jan 2008
by Tom Igoe
This example code is in the public domain.
*/

import processing.serial.*;

Serial serial_port = null;        // The serial port
Table table;

int alignTitle_x = 50;
int alignTitle_y = 65;

int alignLabels_x = 875;
int alignLabels_y = 200;
int labelSpacing = 25;

int alignData_x = alignLabels_x + 165;
int alignData_y = alignLabels_y;
int dataSpacing = labelSpacing;

int boxSize_x = 800;
int boxSize_y = 600;
int alignBox_x = 50;
int alignBox_y = 100;

int alignButton_x = alignData_x-20;
int alignTextButton_x = alignButton_x + 50;
int alignButton_y = alignLabels_y+ 10*labelSpacing;

int alignSerialConnected_x = alignLabels_x;
int alignSerialConnected_y = alignButton_y-20;
int alignSerialConnectedData_x = alignSerialConnected_x+200;

int alignLogging_x = alignLabels_x;
int alignLogging_y = alignSerialConnected_y+170;
int alignLoggingData_x = alignData_x;

Button btn_logging_on;
Button btn_logging_off;

Button btn_serial_up;              // move up through the serial port list
Button btn_serial_dn;              // move down through the serial port list
Button btn_serial_connect;         // connect to the selected serial port
Button btn_serial_disconnect;      // disconnect from the serial port
Button btn_serial_list_refresh;    // refresh the serial port list
String serial_list;                // list of serial ports
int serial_list_index = 0;         // currently selected serial port 
int num_serial_ports = 0;          // number of serial ports in the list

boolean serialConnected = false;
boolean oldSerialConnected = true;

boolean newData = true;
boolean BST = true;

boolean logging = false;
boolean oldLogging = true;
int logIndex = 0;

int millis_tracker = 0;

void setup () {
  
  millis_tracker = millis();
  
  table = new Table();
  
  table.addColumn("id");
  table.addColumn("PC time");
  table.addColumn("fix");
  table.addColumn("time");
  table.addColumn("hours");
  table.addColumn("mins");
  table.addColumn("secs");
  table.addColumn("altitude");
  table.addColumn("latitude");
  table.addColumn("longitude");
  table.addColumn("speed");
  table.addColumn("temperature");
  table.addColumn("pressure");
  table.addColumn("radiation counts");
  
  // set the window size:
  size(1200, 800);        

  // Check the listed serial ports in your machine
  // and use the correct index number in Serial.list()[].

  btn_serial_up = new Button("^", alignButton_x, alignButton_y+10, 40, 20);
  btn_serial_dn = new Button("v", alignButton_x, alignButton_y+50, 40, 20);
  btn_serial_connect = new Button("Connect", alignTextButton_x, alignButton_y+10, 100, 25);
  btn_serial_disconnect = new Button("Disconnect", alignTextButton_x, alignButton_y+45, 100, 25);
  btn_serial_list_refresh = new Button("Refresh", alignTextButton_x, alignButton_y+80, 100, 25);
  btn_logging_on = new Button("On", alignLogging_x+25, alignLogging_y + 20, 100, 25);
  btn_logging_off = new Button("Off", alignLogging_x + 175, alignLogging_y + 20, 100, 25);
  
  // get the list of serial ports on the computer
  serial_list = Serial.list()[serial_list_index];
  
  //println(Serial.list());
  //println(Serial.list().length);
  
  // get the number of serial ports in the list
  num_serial_ports = Serial.list().length;
  
  background(255,255,255);      // set inital background:
  stroke(0,0,0);
  fill(245,245,245);
  rect(alignBox_x,alignBox_y,boxSize_x,boxSize_y);
  
  textAlign(LEFT);
  textSize(25);
  fill(0,0,0);
  text(String.format("ULHAB Data Viewer: %02d/%02d/%04d", day(), month(), year()), alignTitle_x, alignTitle_y);
  
  textSize(20);
  text("Fix:", alignLabels_x, 120);
  text("Latest Data:", alignLabels_x, 160);
  text("Serial Connected:", alignSerialConnected_x, alignSerialConnected_y);
  text("Logging:", alignLogging_x, alignLogging_y);
  
  textSize(18);

  text("Time:",            alignLabels_x, alignLabels_y);
  text("Latitude:",        alignLabels_x, alignLabels_y + labelSpacing);
  text("Longitude:",       alignLabels_x, alignLabels_y + 2*labelSpacing);
  
  fill(altCol[0], altCol[1], altCol[2]);
  text("Altitude (m):",    alignLabels_x, alignLabels_y + 3*labelSpacing);
  
  fill(speedCol[0], speedCol[1], speedCol[2]);
  text("Speed (m/s):",     alignLabels_x, alignLabels_y + 4*labelSpacing);
  
  fill(tempCol[0], tempCol[1], tempCol[2]);
  text("Temperature (C):", alignLabels_x, alignLabels_y + 5*labelSpacing);
  
  fill(presCol[0], presCol[1], presCol[2]);
  text("Pressure (mbar):", alignLabels_x, alignLabels_y + 6*labelSpacing);
  
  fill(radCol[0], radCol[1], radCol[2]);
  text("Radiation (CPM):", alignLabels_x, alignLabels_y + 7*labelSpacing);
}

void draw () {
  
  if(millis() - millis_tracker > 10000)
  {
    resetVars();
  }
  
  // draw the buttons in the application window
  btn_serial_up.Draw();
  btn_serial_dn.Draw();
  btn_serial_connect.Draw();
  btn_serial_disconnect.Draw();
  btn_serial_list_refresh.Draw();
  btn_logging_on.Draw();
  btn_logging_off.Draw();
  // draw the text box containing the selected serial port
  DrawTextBox("Select Port", serial_list, alignLabels_x, alignButton_y+10, 120, 60);
  
  textSize(20);
  
  /* Fix or not */
  if(fix != oldFix)
  {
    stroke(255,255,255);
    fill(255,255,255);
    rect(alignData_x,100,70,40);
    stroke(0,0,0);
    if(fix == 1)
    {
      fill(10,180,10);
      text("True", alignData_x, 120);
    }
    else
    {
      fill(255,0,0);
      text("False", alignData_x, 120);
    }
    
    oldFix=fix;
  }
  
  /* Connected or not */
  if(serialConnected != oldSerialConnected)
  {
    stroke(255,255,255);
    fill(255,255,255);
    rect(alignSerialConnectedData_x,alignSerialConnected_y-25,70,40);
    stroke(0,0,0);
    if(serialConnected)
    {
      fill(10,180,10);
      text("True", alignSerialConnectedData_x, alignSerialConnected_y);
    }
    else
    {
      fill(255,0,0);
      text("False", alignSerialConnectedData_x, alignSerialConnected_y);
    }
    
    oldSerialConnected=serialConnected;
  }
  
  /* Connected or not */
  if(logging != oldLogging)
  {
    stroke(255,255,255);
    fill(255,255,255);
    rect(alignLoggingData_x,alignLogging_y-25,70,40);
    stroke(0,0,0);
    if(logging)
    {
      fill(10,180,10);
      text("True", alignLoggingData_x, alignLogging_y);
    }
    else
    {
      NewTable();
      fill(255,0,0);
      text("False", alignLoggingData_x, alignLogging_y);
    }
    
    oldLogging=logging;
  }
  
  /* Have we got any new data ? */
  if(newData)
  {
  
    //Erase the current data
    stroke(255,255,255);
    fill(255,255,255);
    rect(alignData_x,alignData_y-dataSpacing,width-alignData_x,9*dataSpacing);
    
    stroke(0,0,0);
    fill(0,0,0);
    
    textSize(18);
    
    if(fix == 1)
      text(String.format("%02d:%02d:%02d", hours, mins, secs),     alignData_x, alignData_y);
    else
      text("--:--:--",   alignData_x, alignData_y);
    
    text(String.format("%.6f", lat),        alignData_x, alignData_y + dataSpacing);
    text(String.format("%.6f", lon),        alignData_x, alignData_y + 2*dataSpacing);
    text(String.format("%.2f", alt),        alignData_x, alignData_y + 3*dataSpacing);
    text(String.format("%.2f", speed),      alignData_x, alignData_y + 4*dataSpacing);
    text(String.format("%.2f", temp),       alignData_x, alignData_y + 5*dataSpacing);
    text(String.format("%.1f", pres),       alignData_x, alignData_y + 6*dataSpacing);
    text(rad_counts,                        alignData_x, alignData_y + 7*dataSpacing);
    
    newData = false;
  }
  
  DrawPlot();
  
}

void resetVars()
{
   time = 0;
   hours = 0;
   mins = 0;
   secs = 0;
   fix = 0;
   oldFix = -1;
   lat = 0;
   lon = 0;
   speed = 0;
   alt = 0;
   pres = 0;
   temp = 0;
   rad_counts = 0;
   newData = true;
}

void serialEvent (Serial serial_port) {
  
  try {
    
      // get the ASCII string:
    String inString = serial_port.readStringUntil('\n');
    if (inString != null) {
      println(inString);
      
      String[] str = split(inString, ',');
      fix = int(str[0]);
      time = int(str[1]);
      alt = float(str[2]);
      lat = float(str[3]);
      lon = float(str[4]);
      speed = float(str[5]);
      temp = float(str[6]);
      pres = float(str[7]);
      rad_counts = int(str[8]);
      
      /* Darn BST */
      if(BST)
      {
        time += 3600;
        if(time > 86400)
          time -= 86400;
      }
      
      CalculateMaxMins();
      
      hours = time / 3600;
      mins = (time % 3600) / 60;
      secs = (time % 3600) % 60;
      
      println(fix, ' ', time, ' ', alt, ' ', lat, ' ', lon, ' ', speed, ' ', temp, ' ', pres, ' ', rad_counts);
      println();
      
      newData = true;
      millis_tracker = millis();
      
      FillTable();
    }
  }
  catch(RuntimeException e) {
    println();
    e.printStackTrace();
    println();
  }
}

void mousePressed() {
  // up button clicked
  if (btn_serial_up.MouseIsOver()) {
    if (serial_list_index > 0) {
      // move one position up in the list of serial ports
      serial_list_index--;
      serial_list = Serial.list()[serial_list_index];
    }
  }
  // down button clicked
  if (btn_serial_dn.MouseIsOver()) {
    if (serial_list_index < (num_serial_ports - 1)) {
      // move one position down in the list of serial ports
      serial_list_index++;
      serial_list = Serial.list()[serial_list_index];
    }
  }
  // Connect button clicked
  if (btn_serial_connect.MouseIsOver()) {
    if (serial_port == null) {
      // connect to the selected serial port
      serial_port = new Serial(this, Serial.list()[serial_list_index], 9600);
      // A serialEvent() is generated when a newline character is received :
      serial_port.bufferUntil('\n');
      serialConnected = true;
    }
  }
  // Disconnect button clicked
  if (btn_serial_disconnect.MouseIsOver()) {
    if (serial_port != null) {
      // disconnect from the serial port
      serial_port.stop();
      serial_port = null;
      serialConnected = false;
      resetVars();
    }
  }
  // Refresh button clicked
  if (btn_serial_list_refresh.MouseIsOver()) {
    // get the serial port list and length of the list
    serial_list = Serial.list()[serial_list_index];
    num_serial_ports = Serial.list().length;
  }
  
  if(btn_logging_on.MouseIsOver()) {
    logging = true;
  }
  
  if(btn_logging_off.MouseIsOver()) {
    logging = false;
  }
}

// function for drawing a text box with title and contents
void DrawTextBox(String title, String str, int x, int y, int w, int h)
{
  fill(255);
  rect(x, y, w, h);
  fill(0);
  textAlign(LEFT);
  textSize(14);
  text(title, x + 10, y + 10, w - 20, 20);
  textSize(12);  
  text(str, x + 10, y + 40, w - 20, h - 10);
}
