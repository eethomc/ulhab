/*******************************************************************************
 * File:                    gps.c
 * Project:                 ULHAB GPS
 * Date:                    27/11/2018
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             GPS Interfacing
 ******************************************************************************/

//For robust compiling
#ifdef __cplusplus
 extern "C" {
#endif

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "gps.h"
#include "main.h"
#include "usart.h"
#include "string.h"
#include "NMEA.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/


/*******************************************************************************
 * Module globals
 ******************************************************************************/
 /* Receive buffer for DMA */
 extern volatile uint8_t DMA_RX_buffer[DMA_RX_BUFFER_SIZE];
 extern volatile uint8_t NMEA_string_buffer[DMA_RX_BUFFER_SIZE];
 extern volatile bool NMEA_string_rdy;
 uint8_t *prev = (uint8_t *)DMA_RX_buffer;

/*******************************************************************************
 * Functions
 ******************************************************************************/

/* Callback for the idle function */
void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance != GPS_UART) return;

	//ResetDMA(huart);
	HAL_UART_DMAPause(huart);

	uint8_t *current = (uint8_t *)DMA_RX_buffer + (DMA_RX_BUFFER_SIZE-1 - huart->hdmarx->Instance->CNDTR);

	if(current > prev)
	{
		uint8_t size = current - prev + 1;
		memcpy((uint8_t *)NMEA_string_buffer, (void *)prev, SIZE_UINT8(size));
		//memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);
	}
	else if (prev > current)
	{
		uint8_t endSlice = (DMA_RX_buffer + DMA_RX_BUFFER_SIZE) - prev;
		uint8_t startSlice = (current - DMA_RX_buffer +1 );
		memcpy((uint8_t *)NMEA_string_buffer, (void *)prev, SIZE_UINT8(endSlice));
		memcpy((uint8_t *)NMEA_string_buffer+SIZE_UINT8(endSlice), (void *)DMA_RX_buffer, SIZE_UINT8(startSlice));
	}

	prev = ++current;

	NMEA_string_rdy = TRUE;
 	//huart->hdmarx->Instance->CCR |= 0x01; //Set enable bit
 	HAL_UART_DMAResume(huart);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	/* Do nothing */
	return;

	/*ResetDMA(huart);
	HAL_UART_DMAPause(huart);

	memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);

	//huart->hdmarx->Instance->CCR |= 0x01; //Set enable bit
	HAL_UART_DMAResume(huart);*/
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart)
{
	memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);
	HAL_UART_Receive_DMA(&huart1, (void *)DMA_RX_buffer, DMA_RX_BUFFER_SIZE);
}


//For robust compiling
#ifdef __cplusplus
}
#endif
