/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins
     PE0   ------> FMC_NBL0
     PB4 (NJTRST)   ------> SPI1_MISO
     PA15 (JTDI)   ------> SPI1_NSS
     PH14   ------> DCMI_D4
     PI7   ------> DCMI_D7
     PE1   ------> FMC_NBL1
     PB5   ------> SPI1_MOSI
     PG9   ------> FMC_NE2
     PD0   ------> FMC_D2_DA2
     PI6   ------> S_TIM8_CH2
     PI2   ------> SPI2_MISO
     PI1   ------> SPI2_SCK
     PH15   ------> TIM8_CH3N
     PH12   ------> DCMI_D3
     PD4   ------> FMC_NOE
     PD1   ------> FMC_D3_DA3
     PH13   ------> TIM8_CH1N
     PE4   ------> SAI1_FS_A
     PE3   ------> SAI1_SD_B
     PE2   ------> SAI1_MCLK_A
     PB9   ------> S_TIM4_CH4
     PD5   ------> FMC_NWE
     PD2   ------> SDMMC1_CMD
     PC10   ------> SDMMC1_D2
     PI4   ------> DCMI_D5
     PH9   ------> DCMI_D0
     PA12   ------> USB_OTG_FS_DP
     PE6   ------> SAI1_SD_A
     PE5   ------> DCMI_D6
     PD6   ------> USART2_RX
     PC11   ------> SDMMC1_D3
     PI5   ------> DCMI_VSYNC
     PA11   ------> USB_OTG_FS_DM
     PF2   ------> FMC_A2
     PF1   ------> FMC_A1
     PF0   ------> FMC_A0
     PD7   ------> FMC_NE1
     PC12   ------> SDMMC1_CK
     PA10   ------> USB_OTG_FS_ID
     PF3   ------> FMC_A3
     PF4   ------> FMC_A4
     PF5   ------> FMC_A5
     PA8   ------> LPTIM2_OUT
     PC9   ------> SDMMC1_D1
     PC8   ------> SDMMC1_D0
     PC7   ------> S_DATAIN3DFSDM1
     PF10   ------> ADC3_IN13
     PG1   ------> FMC_A11
     PE10   ------> FMC_D7_DA7
     PB11   ------> QUADSPI_BK1_NCS
     PD15   ------> FMC_D1_DA1
     PC0   ------> ADCx_IN1
     PC1   ------> ADCx_IN2
     PC2   ------> S_CKOUTDFSDM1
     PG0   ------> FMC_A10
     PE9   ------> FMC_D6_DA6
     PE15   ------> FMC_D12_DA12
     PG5   ------> FMC_A15
     PG4   ------> FMC_A14
     PG3   ------> FMC_A13
     PG2   ------> FMC_A12
     PD10   ------> FMC_D15_DA15
     PA5   ------> SPI1_SCK
     PB0   ------> QUADSPI_BK1_IO1
     PF15   ------> FMC_A9
     PE8   ------> FMC_D5_DA5
     PE14   ------> FMC_D11_DA11
     PH4   ------> I2C2_SCL
     PD14   ------> FMC_D0_DA0
     PD12   ------> FMC_A17_ALE
     PD11   ------> FMC_A16_CLE
     PD13   ------> FMC_A18
     PA4   ------> ADCx_IN9
     PA7   ------> QUADSPI_BK1_IO2
     PB1   ------> QUADSPI_BK1_IO0
     PF14   ------> FMC_A8
     PE7   ------> FMC_D4_DA4
     PE13   ------> FMC_D10_DA10
     PH5   ------> DCMI_PIXCLK
     PD9   ------> FMC_D14_DA14
     PD8   ------> FMC_D13_DA13
     PA3   ------> QUADSPI_CLK
     PA6   ------> QUADSPI_BK1_IO3
     PF13   ------> FMC_A7
     PE12   ------> FMC_D9_DA9
     PH10   ------> DCMI_D1
     PH11   ------> DCMI_D2
     PB15   ------> SPI2_MOSI
     PB14   ------> I2C2_SDA
     PA2   ------> USART2_TX
     PA1   ------> ADCx_IN6
     PF12   ------> FMC_A6
     PE11   ------> FMC_D8_DA8
     PB10   ------> SAI1_SCK_A
     PH8   ------> DCMI_HSYNC
     PB12   ------> S_DATAIN1DFSDM1
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  HAL_PWREx_EnableVddIO2();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);


  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = COUNT_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(COUNT_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED1_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
