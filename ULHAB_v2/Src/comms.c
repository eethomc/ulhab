/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		comms.c
 * Project:		ULHAB_v2
 * Date:        11 Apr 2019
 * Author:      Thom
 * Target:      
 * Copyright:   Doayee 2019
 * Description: 
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "comms.h"
#include "NMEA.h"
#include "MS5607.h"
#include "stdio.h"
#include "usart.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/
tx_packet_t store = {0};
uint8_t data_to_send = 0;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/

bool parse_nmea_into_packet(NMEA_information_t *nmea)
{
	if(nmea == NULL) return FALSE;

	store.time_fix.tf.fix = nmea->valid;

	if(nmea->valid)
		store.time_fix.tf.time = nmea->time;
	else
		store.time_fix.tf.time = 0;

	if(nmea->altitude > 1.0)
		store.altitude.f = nmea->altitude;

	store.latitude.f = nmea->latitude;
	store.longitude.f = nmea->longitude;
	store.speed.f = nmea->speed;

	return TRUE;
}

bool parse_ms5607_res_into_packet(MS5607_results_t *res)
{
	if(res == NULL) return FALSE;

	store.pressure.f = res->pressure;
	store.temperature.f = res->temp;

	return TRUE;
}

bool parse_rad_count_into_packet(uint32_t count)
{
	store.rad_counts.u = count;

	return TRUE;
}

bool transmit_packet()
{
#ifdef OUTPUT_PRETTY

	uint8_t buf[TX_PACKET_SIZE] = {0};

	sprintf((char *)buf, "Fix:    %d\nTime:   %d\nAlt:    %.01f\nLat:    %f\nLon:    %f\nSpd:    %f\nTemp:   %.02f\nPres:   %.01f\nCounts: %li\n\n",
			store.time_fix.tf.fix, store.time_fix.tf.time, store.altitude.f, store.latitude.f, store.longitude.f, store.speed.f,	//GPS
			store.temperature.f, store.pressure.f,												//MS5607
			store.rad_counts.u);
	//Rad
	HAL_UART_Transmit(&hlpuart1, buf, strlen((char *)buf), 0xFFFF);

#else
	uint8_t buf[sizeof(pkt_message_t)] = {0};
	pkt_message_t msg = {0};
	msg.adr = DEST_ADR;

	if(++data_to_send % 2 == 0)
	{
		msg.data_id = data_to_send/2;
		switch(data_to_send/2)
		{
		case PKT_LAT:
			memcpy(&(msg.payload), &(store.latitude), sizeof(pkt_payload_u));
			break;

		case PKT_LON:
			memcpy(&(msg.payload), &(store.longitude), sizeof(pkt_payload_u));
			break;

		case PKT_SPD:
			memcpy(&(msg.payload), &(store.speed), sizeof(pkt_payload_u));
			break;

		case PKT_ALT:
			memcpy(&(msg.payload), &(store.altitude), sizeof(pkt_payload_u));
			break;

		case PKT_TEMP:
			memcpy(&(msg.payload), &(store.temperature), sizeof(pkt_payload_u));
			break;

		case PKT_PRES:
			memcpy(&(msg.payload), &(store.pressure), sizeof(pkt_payload_u));
			break;

		case PKT_RAD_CNT:
			memcpy(&(msg.payload), &(store.rad_counts), sizeof(pkt_payload_u));
			break;

		case PKT_MAX:

		default:
			data_to_send = 0;
			return transmit_packet();
		}
	}
	else
	{
		msg.data_id = PKT_TIME_FIX;
		memcpy(&(msg.payload), &(store.time_fix), sizeof(pkt_payload_u));
	}


	/* Find the checksum */
	uint8_t check_buf[8] = {0};
	memcpy(check_buf, &msg, SIZE_UINT8(6));

	for(uint8_t i = 0; i < 6; i++)
	{
		buf[6] += buf[i];
		buf[7] += buf[6];
	}

	msg.check[0] = buf[6];
	msg.check[1] = buf[7];

	msg.end[0] = msg.data_id;
	msg.end[1] = DEST_ADR;

	memcpy(buf, &msg, sizeof(pkt_message_t));
	HAL_UART_Transmit(&hlpuart1, buf, sizeof(pkt_message_t), 0xFFFF);

#endif

	//Upddate the time
	if(store.time_fix.tf.fix)
		store.time_fix.tf.time += TX_TIMER_PERIOD/1000;

	return TRUE;
}
