/*******************************************************************************
 * File:                    NMEA.c
 * Project:                 ULHAB GPS
 * Date:                    27/11/2018
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             NMEA String Decoding
 ******************************************************************************/

//For robust compiling

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "NMEA.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/
static uint8_t *NMEA_FindFieldOfString(NMEA_string_t *str, uint8_t index);
static float NMEA_ConvertLatLonToDecimalDegrees(float original);

bool NMEA_Parse_String(NMEA_information_t *info)
{
	NMEA_queue_t *strings = NMEA_DecodeRawString((uint8_t *)NMEA_string_buffer, (uint8_t *)NMEA_string_buffer+SIZE_UINT8(DMA_RX_BUFFER_SIZE-1));

	if(strings != NULL)
	{
		strings = NMEA_FilterQueueOnChecksum(strings);
		NMEA_UpdateInformationFromQueue(info, strings);
	}

	NMEA_FreeQueue(strings);
	NMEA_string_rdy = FALSE;

	return (strings == NULL ? FALSE : TRUE);
}

void NMEA_UpdateInformationFromQueue(NMEA_information_t *info, NMEA_queue_t *node)
{
	if(node == NULL) return;

	NMEA_UpdateInformation(info, node->payload);

	NMEA_UpdateInformationFromQueue(info, node->next);

	return;
}

bool NMEA_UpdateInformation(NMEA_information_t *info, NMEA_string_t *str)
{
	NMEA_std_msg_et msg = NMEA_DecodeMessageType(str);

	switch (msg) {
		case NMEA_SM_NONE:
		case NMEA_SM_DTM:
		case NMEA_SM_GBS:
		case NMEA_SM_GPQ:
		case NMEA_SM_GRS:
		case NMEA_SM_GSA:
		case NMEA_SM_GST:
		case NMEA_SM_GSV:
		case NMEA_SM_THS:
		case NMEA_SM_TXT:
			return FALSE;
			break;
		case NMEA_SM_GGA:
			return NMEA_UpdateInformationGGA(info, str);
			break;
		case NMEA_SM_GLL:
			return NMEA_UpdateInformationGLL(info, str);
		default:
			break;
	}

	return FALSE;
}

bool NMEA_UpdateInformationGLL(NMEA_information_t *info, NMEA_string_t *str)
{
	/* Check the valid flag */
	uint8_t *valid = NMEA_FindFieldOfString(str, 6);
	if(valid == NULL) return FALSE;

	/* If the data is not valid */
	if(*valid == 'V')
	{
		info->valid = FALSE;
		return FALSE;
	}

	/* Data is valid */
	info->valid = TRUE;

	/* Find the time */
	uint8_t *time = NMEA_FindFieldOfString(str, 5);
	if(time == NULL) return FALSE;

	/* find the times */
	uint8_t timeArr[3] = {0,0,0};

	/* populate the array with numbers */
	for(uint8_t j = 0; j < 3; j++)
	{
		for(uint8_t i = 0; i < 2; i++)
		{
			timeArr[j] *= 10;
			timeArr[j] += *(time++) - '0';
		}
	}

	/* save the time as seconds since 00:00 */
	info->time = 3600*timeArr[0] + 60*timeArr[1] + timeArr[2];

	/* Get the lat and lon */
	uint8_t *lat = NMEA_FindFieldOfString(str, 1);
	if(lat == NULL) return FALSE;
	info->latitude = NMEA_ConvertLatLonToDecimalDegrees(atof((char *)lat));

	uint8_t *lon = NMEA_FindFieldOfString(str, 3);
	if(lon == NULL) return FALSE;
	info->longitude = NMEA_ConvertLatLonToDecimalDegrees(atof((char *)lon));

	uint8_t *n = NMEA_FindFieldOfString(str, 2);
	uint8_t *e = NMEA_FindFieldOfString(str, 4);
	if(n == NULL || e == NULL) return FALSE;
	if(*n != 'N') info->latitude *= -1;
	if(*e != 'E') info->longitude *= -1;

	return TRUE;
}

bool NMEA_UpdateInformationGGA(NMEA_information_t *info, NMEA_string_t *str)
{
	/* Check the fix */
	uint8_t *fix = NMEA_FindFieldOfString(str, 6);
	if(fix == NULL) return FALSE;

	/* If the fix is not valid */
	if(*fix == '0')
	{
		info->valid = FALSE;
		return FALSE;
	}

	/* Data is valid */
	info->valid = TRUE;

	/* Find the time */
	uint8_t *time = NMEA_FindFieldOfString(str, 1);
	if(time == NULL) return FALSE;

	/* find the times */
	uint8_t timeArr[3] = {0,0,0};

	/* populate the array with numbers */
	for(uint8_t j = 0; j < 3; j++)
	{
		for(uint8_t i = 0; i < 2; i++)
		{
			timeArr[j] *= 10;
			timeArr[j] += *(time++) - '0';
		}
	}

	/* save the time as seconds since 00:00 */
	info->time = 3600*timeArr[0] + 60*timeArr[1] + timeArr[2];

	/* Get the lat and lon */
	uint8_t *lat = NMEA_FindFieldOfString(str, 2);
	if(lat == NULL) return FALSE;
	info->latitude = NMEA_ConvertLatLonToDecimalDegrees(atof((char *)lat));

	uint8_t *lon = NMEA_FindFieldOfString(str, 4);
	if(lon == NULL) return FALSE;
	info->longitude = NMEA_ConvertLatLonToDecimalDegrees(atof((char *)lon));

	uint8_t *n = NMEA_FindFieldOfString(str, 3);
	uint8_t *e = NMEA_FindFieldOfString(str, 5);
	if(n == NULL || e == NULL) return FALSE;
	if(*n != 'N') info->latitude *= -1;
	if(*e != 'E') info->longitude *= -1;

	/* Get the altitude */
	uint8_t *alt = NMEA_FindFieldOfString(str, 9);
	if(alt == NULL) return FALSE;
	info->altitude = atof((char *)alt);

	/* Get the number of sats */
	uint8_t *sats = NMEA_FindFieldOfString(str, 7);
	if(sats == NULL) return FALSE;
	info->satelites = (*sats) - '0';

	return TRUE;
}

static float NMEA_ConvertLatLonToDecimalDegrees(float original)
{
	int degrees = (int)(original / 100);
	float mins_secs = original - 100*((float)degrees);
	mins_secs /= 60;
	float to_return = mins_secs + (float)degrees;
	return to_return;
}

static uint8_t *NMEA_FindFieldOfString(NMEA_string_t *str, uint8_t index)
{
	/* We will increment this to match index */
	uint8_t i = 0;
	uint8_t *ptr = str->str;

	while(i < index)
	{
		while(*(++ptr) != ',')
		{
			/* The given index is too high */
			if(ptr > str->str + str->len) return NULL;
			if(ptr == NULL) return NULL;
			if(*ptr == '\0' || *ptr == '\r' || *ptr == '*') return NULL;
		}

		/* If we have the correct index */
		if(++i == index) return (++ptr);
	}

	return NULL;
}

NMEA_queue_t *NMEA_FilterQueueOnChecksum(NMEA_queue_t *node)
{
	/* Check we have a valid node */
	if(node == NULL) return NULL;

	/* Assign the working pointer to the start of the string */
	uint8_t *ptr = node->payload->str;

	uint8_t checksum = 0;

	/* For every character in the string before the '*' character */
	while(*(++ptr) != '*')
	{
		/* Check we are still in a valid region */
		if(*ptr == '\0') return NULL;
		if(ptr >= node->payload->str + node->payload->len) return NULL;

		/* Calculate the checksum */
		checksum ^= *ptr;
	}

	uint8_t NMEA_checksum = 0;

	/* ptr now points at the '*' char */
	while(*(++ptr) != '\r')
	{
		/* Check we are still in a valid region */
		if(*ptr == '\0') return NULL;
		if(ptr >= node->payload->str + node->payload->len) return NULL;

		/* Shift the checksum up by 1 nibble */
		NMEA_checksum <<= 4;

		/* OR in the nibble */
		NMEA_checksum |= (*ptr <= '9' ? *ptr - '0' : *ptr - 'A');
	}

	/* This checksum is bad, throw the node away */
	if(NMEA_checksum != checksum)
	{
		NMEA_queue_t *next = node->next;
		NMEA_FreeNode(node);
		return NMEA_FilterQueueOnChecksum(next);
	}

	/* Checksum is valid - perform the check down the queue and return the
	 * node
	 */
	node->next = NMEA_FilterQueueOnChecksum(node->next);
	return node;
}

/*******************************************************************************
 * Name:   	NMEA_queue_t *NMEA_DecodeRawString(uint8_t *start, uint8_t *end)
 * Inputs: 	uint8_t *start - pointer to the start of the string to decode
 * 			uint8_t *end - pointer to the final character of the string
 * Return: 	NMEA_queue_t* - pointer to the first node of the queue
 * Notes:  	Takes a string and finds all valid NMEA strings in it, cuts each
 * 			out of the string and places it in a queue of NMEA_string_t types.
 ******************************************************************************/
NMEA_queue_t *NMEA_DecodeRawString(uint8_t *start, uint8_t *end)
{
	if(start == NULL || end == NULL) return NULL;

	/* The following block creates a local buffer to hold the string in -
	 * this prevents the data being overwritten by the UART IDLE interrupt.
	 * May be unneeded if the function is always quick enough to service the
	 * string before the next NMEA string is received from the GPS.
	 * See: USE_LOCAL_BUFFER in NMEA.h
	 */

#ifdef USE_LOCAL_BUFFER
	uint8_t workingStr[DMA_RX_BUFFER_SIZE];
	memcpy(workingStr, start, SIZE_UINT8(end - start +1));
	end = start + SIZE_UINT8(end - start);
	start = workingStr;
#endif

	/* Search for first '$' */
	while(*start != '$')
	{
		if(*start == '\0') return NULL;
		if(++start >= end) return NULL;
	}

	/* Search for following '\n' */
	uint8_t *ptr = start;
	while(*ptr != '\n')
	{
		if(*ptr == '\0') return NULL;
		if(++ptr >= end) return NULL;
	}

	/* Get ourselves a queue object */
	NMEA_queue_t *node = malloc(sizeof(NMEA_queue_t));
	if(node == NULL) return NULL;

	/* Get ourselves a string object */
	NMEA_string_t *payload = malloc(sizeof(NMEA_string_t));
	if(payload == NULL)
	{
		free(node);
		return NULL;
	}

	/* Get ourselves the required space for the string */
	uint8_t *str = malloc(SIZE_UINT8((++ptr)-start));
	if(str == NULL)
	{
		free(payload);
		free(node);
		return NULL;
	}

	/* Copy the NMEA string */
	payload->len = (ptr-start);
	memcpy(str, start, SIZE_UINT8(payload->len));
	payload->str = str;

	node->payload = payload;
	node->next = NMEA_DecodeRawString(ptr, end);
	return node;
}

/*******************************************************************************
 * Name:   	void NMEA_FreeQueue(NMEA_queue_t *node)
 * Inputs: 	NMEA_queue_t *node - pointer to the queue to free
 * Return: 	None.
 * Notes:  	Free's the queue and all it's payloads.
 ******************************************************************************/
void NMEA_FreeQueue(NMEA_queue_t *node)
{
	/* If we are at the end of the queue */
	if(node == NULL) return;

	/* free the next queue object */
	NMEA_FreeQueue(node->next);

	/* Free the node and all parts of the payload */
	NMEA_FreeNode(node);

	return;
}

void NMEA_FreeNode(NMEA_queue_t *node)
{
	if(node == NULL) return;

	free(node->payload->str);
	free(node->payload);
	free(node);

	return;
}

/*******************************************************************************
 * Name:   	NMEA_std_msg_et NMEA_DecodeMessageType(uint8_t *start, uint8_t length)
 * Inputs: 	uint8_t *start - pointer to the start of the string
 * 			uint8_t length - the length of the string
 * Return: 	NMEA_std_msg_et - the message type
 * Notes:  	Examines the NMEA string start.
 ******************************************************************************/
NMEA_std_msg_et NMEA_DecodeMessageType(NMEA_string_t *nmea)
{
	/* Cannot have a valid command in this case */
	if(nmea->len < 6) return NMEA_SM_NONE;

	volatile uint32_t command;

	/* Copy the first 3 chars after the '$GP' */
	memcpy((void *)&command, (void *)(nmea->str + SIZE_UINT8(3)), SIZE_UINT8(3));

	/* Compare them with bitwise matches */
	switch(command & 0x00FFFFFF)
	{
		case DTM:
			return NMEA_SM_DTM;
		case GBS:
			return NMEA_SM_GBS;
		case GGA:
			return NMEA_SM_GGA;
		case GLL:
			return NMEA_SM_GLL;
		case GPQ:
			return NMEA_SM_GPQ;
		case GRS:
			return NMEA_SM_GRS;
		case GSA:
			return NMEA_SM_GSA;
		case GST:
			return NMEA_SM_GST;
		case GSV:
			return NMEA_SM_GSV;
		case RMC:
			return NMEA_SM_RMC;
		case THS:
			return NMEA_SM_THS;
		case TXT:
			return NMEA_SM_TXT;
		case VTG:
			return NMEA_SM_VTG;
		case ZDA:
			return NMEA_SM_ZDA;
		default:
			return NMEA_SM_NONE;
	}

	/* Keeps the compiler happy */
	return NMEA_SM_NONE;
}


