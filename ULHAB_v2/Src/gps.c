/*******************************************************************************
 * File:                    gps.c
 * Project:                 ULHAB GPS
 * Date:                    27/11/2018
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             GPS Interfacing
 ******************************************************************************/

//For robust compiling
#ifdef __cplusplus
 extern "C" {
#endif

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "gps.h"
#include "main.h"
#include "usart.h"
#include "string.h"
#include "NMEA.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/


/*******************************************************************************
 * Module globals
 ******************************************************************************/
 /* Receive buffer for DMA */
 volatile uint8_t DMA_RX_buffer[DMA_RX_BUFFER_SIZE];
 volatile uint8_t NMEA_string_buffer[DMA_RX_BUFFER_SIZE];
 volatile bool NMEA_string_rdy;
 uint8_t *prev = (uint8_t *)DMA_RX_buffer;
 void (*cb)(void) = NULL;

 /*static uint8_t ubloxConfigNav[] =      {0xB5,0x62,  			// Header
                                         0x06,0x24,  			// ID
                                         0x24,0x00,				// Length = 36
 										0x00,0x00, 				// Mask
 										0x07,					// dynModel
 										0x03,					// fixMode
 										0x00,0x00,0x00,0x00,	// fixedAlt
 										0x10,0x27,0x00,0x00,  	// fixedAltVar
 										0x05,					// minElev
 										0x00,					// drLimit
 										0xFA,0x00,				// pDop
 										0xFA,0x00,				// tDop
 										0x64,0x00,				// pAcc
 										0x2C,0x01,				// tAcc
 										0x00,					// staticHoldThresh
 										0x3C,					// dgpsTimeOut
 										0x00,0x00,0x00,0x00,	// reserved
 										0x00,0x00,0x00,0x00,	// reserved
 										0x00,0x00,0x00,0x00		// reserved
 										};*/

 static uint8_t ubloxInitData[] = {0xB5,0x62,            // Header             Turn On NMEA GGA Msg
                                   0x06,0x01,            // ID
                                   0x03,0x00,            // Length
                                   0xF0,                 // Msg Class    NMEA
                                   0x00,                 // Msg ID       GGA
                                   0x01,                 // Rate         1
                                   0xFA,0x0F,            // CK_A, CK_B

                                   0xB5,0x62,            // Header             Turn On NMEA GLL Msg
                                   0x06,0x01,            // ID
                                   0x03,0x00,            // Length
                                   0xF0,                 // Msg Class    NMEA
                                   0x01,                 // Msg ID       GLL
                                   0x01,                 // Rate         0
                                   0xFB,0x11,            // CK_A, CK_B

                                   0xB5,0x62,            // Header             Turn Off NMEA GSA Msg
                                   0x06,0x01,            // ID
                                   0x03,0x00,            // Length
                                   0xF0,                 // Msg Class    NMEA
                                   0x02,                 // Msg ID       GSA
                                   0x00,                 // Rate         0
                                   0xFC,0x13,            // CK_A, CK_B

                                   0xB5,0x62,            // Header             Turn Off NMEA GSV Msg
                                   0x06,0x01,            // ID
                                   0x03,0x00,            // Length
                                   0xF0,                 // Msg Class    NMEA
                                   0x03,                 // Msg ID       GSV
                                   0x00,                 // Rate         0
                                   0xFD,0x15,            // CK_A, CK_B

                                   0xB5,0x62,            // Header             Turn Off NMEA RMC Msg
                                   0x06,0x01,            // ID
                                   0x03,0x00,            // Length
                                   0xF0,                 // Msg Class    NMEA
                                   0x04,                 // Msg ID       RMC
                                   0x00,                 // Rate         0
                                   0xFE,0x17,            // CK_A, CK_B

                                   0xB5,0x62,            // Header             Turn Off NMEA VTG Msg
                                   0x06,0x01,            // ID
                                   0x03,0x00,            // Length
                                   0xF0,                 // Msg Class    NMEA
                                   0x05,                 // Msg ID       VTG
                                   0x00,                 // Rate         0
                                   0xFF,0x19,            // CK_A, CK_B


                                   0xB5,0x62,            // Header             Setup SBAS Mode
                                   0x06,0x16,            // ID
                                   0x08,0x00,            // Length
                                   0x01,                 // Mode
                                   0x07,                 // Usage
                                   0x03,                 // Max SBAS
                                   0x00,                 // Scan Mode 2
                                   0x04,0xE0,0x04,0x00,  // Scan Mode 1
                                   0x17,0x8D,            // CK_A, CK_B

								   0xB5,0x62,			 // Header			CFG-RXM
								   0x06,0x11,			 // ID
								   0x02,0x00,			 // Length
								   0x08,				 // Reserved
								   0x01,				 // LP Mode
								   0x22,0x92,			 // CK_A, CK_B

								   0xB5,0x62,            // Header             Set Navigation Engine Settings
								   0x06,0x24,            // ID
								   0x24,0x00,            // Length
								   0x01,0x00,            // Mask                 Mask to apply only dynamic model setting
								   0x07,                 // DynModel             Airborne with < 2g Acceleration
								   0x03,                 // FixMode              Auto 2D/3D
								   0x00,0x00,0x00,0x00,  // FixedAlt               0 scaled at 0.01
								   0x10,0x27,0x00,0x00,  // FixedAltVar            1 scaled at 0.0001
								   0x05,                 // MinElev                5 deg
								   0x00,                 // DrLimit                0 s
								   0xFA,0x00,            // pDop                  25 scaled at 0.1
								   0xFA,0x00,            // tDop                  25 scaled at 0.1
								   0x64,0x00,            // pAcc                 100 m
								   0x2C,0x01,            // tAcc                 300 m
								   0x00,                 // StaticHoldThresh       0 m/s
								   0x3C,                 // DgpsTimeOut           60 s
								   0x00,0x00,0x00,0x00,  // Reserved2
								   0x00,0x00,0x00,0x00,  // Reserved3
								   0x00,0x00,0x00,0x00,  // Reserved4
								   0x56,0x75,            // CK_A, CK_B


                                   /*0xB5,0x62,            // Header             Setup Meaurement Rates, Clock Reference
                                   0x06,0x08,            // ID
                                   0x06,0x00,            // Length
                                   0xC4,0x09,            // Measurement Rate     1000 mSec (1 Hz)
                                   0x01,0x00,            // Navigation Rate      1 Measurement Cycle
                                   0x00,0x00,            // Time Reference       UTM Time
								   0xE2,0x7D*/	}; 		 // CK_A, CK_B

/*******************************************************************************
 * Functions
 ******************************************************************************/

/* Callback for the idle function */
void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance != GPS_UART) return;

	//ResetDMA(huart);
	HAL_UART_DMAPause(huart);

	uint8_t *current = (uint8_t *)DMA_RX_buffer + (DMA_RX_BUFFER_SIZE-1 - huart->hdmarx->Instance->CNDTR);

	if(current > prev)
	{
		uint8_t size = current - prev + 1;
		memcpy((uint8_t *)NMEA_string_buffer, (void *)prev, SIZE_UINT8(size));
		//memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);
	}
	else if (prev > current)
	{
		uint8_t endSlice = (DMA_RX_buffer + DMA_RX_BUFFER_SIZE) - prev;
		uint8_t startSlice = (current - DMA_RX_buffer +1 );
		memcpy((uint8_t *)NMEA_string_buffer, (void *)prev, SIZE_UINT8(endSlice));
		memcpy((uint8_t *)NMEA_string_buffer+SIZE_UINT8(endSlice), (void *)DMA_RX_buffer, SIZE_UINT8(startSlice));
	}

	prev = ++current;

	if(cb != NULL)
		cb();

 	//huart->hdmarx->Instance->CCR |= 0x01; //Set enable bit
 	HAL_UART_DMAResume(huart);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	/* Do nothing */
	return;

	/*ResetDMA(huart);
	HAL_UART_DMAPause(huart);

	memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);

	//huart->hdmarx->Instance->CCR |= 0x01; //Set enable bit
	HAL_UART_DMAResume(huart);*/
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart)
{
	memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);
	HAL_UART_Receive_DMA(&huart1, (void *)DMA_RX_buffer, DMA_RX_BUFFER_SIZE);
}

void GPS_Init(void (*callback)(void))
{
	if(callback == NULL) return;

	cb = callback;

	//while(HAL_UART_Transmit(&huart1, ubloxInitData, sizeof(ubloxInitData), 0xFFFF) != HAL_OK);

	HAL_UART_Receive_DMA(&huart1, (void *)DMA_RX_buffer, DMA_RX_BUFFER_SIZE);
}


//For robust compiling
#ifdef __cplusplus
}
#endif
