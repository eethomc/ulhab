/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		threads.c
 * Project:
 * Date:        20/02/2019
 * Author:      Thomas Cousins
 * Target:      STM32L496
 * Description: Contains all the seperate threads.
 ******************************************************************************/

/*******************************************************************************
* Includes
*******************************************************************************/
#include "threads.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/
#define GPS_QUEUE_LENGTH 	( 1 )
#define GPS_QUEUE_SIZE 		( SIZE_UINT8( DMA_RX_BUFFER_SIZE ))

#define CAM_BUF_SIZE 0x7080

/*******************************************************************************
 * System globals
 ******************************************************************************/
volatile uint8_t DMA_RX_buffer[DMA_RX_BUFFER_SIZE];
uint16_t pBuffer[CAM_BUF_SIZE];
extern DCMI_HandleTypeDef hDcmiHandler;

/*******************************************************************************
 * Module globals
 ******************************************************************************/
osThreadId GPSThreadHandle;
osThreadId MS5607ThreadHandle;
osThreadId CameraThreadHandle;

NMEA_information_t info = {0};

/*******************************************************************************
 * Thread Prototypes
 ******************************************************************************/
static void thr_MS5607(void const *argument);
static void thr_GPS(void const *argument);
static void thr_camera(void const *argument);

/*******************************************************************************
 * Thread Definition Function
 ******************************************************************************/
void DefineThreads(void)
{
	/* Thread definitions */
	osThreadDef(GPS_Thread, thr_GPS, osPriorityAboveNormal, 0, configMINIMAL_STACK_SIZE);
	osThreadDef(MS5607_Thread, thr_MS5607, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	osThreadDef(Camera_Thread, thr_camera, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);

	/* Start threads */
	GPSThreadHandle = osThreadCreate(osThread(GPS_Thread), NULL);
	MS5607ThreadHandle = osThreadCreate(osThread(MS5607_Thread), NULL);
	CameraThreadHandle = osThreadCreate(osThread(Camera_Thread), NULL);
}

/*******************************************************************************
 * Threads
 ******************************************************************************/

/*******************************************************************************
 * Name:   static void thr_GPS(void const *argument)
 * Inputs: void const *argument - nothing in this context
 * Return: None
 * Notes:  Handles the GPS communication
 ******************************************************************************/
void thr_GPS(void const *argument)
{
	(void) argument;

	GPS_Init();

	uint8_t NMEA_string_buffer[DMA_RX_BUFFER_SIZE];
	 uint8_t *prev = (uint8_t *)DMA_RX_buffer;

	HAL_UART_Receive_DMA(&huart1, (void *)DMA_RX_buffer, DMA_RX_BUFFER_SIZE);

	NMEA_queue_t *strings;

	for (;;)
	{

		ulTaskNotifyTake( pdTRUE, portMAX_DELAY );

		//Pause DMA
		HAL_UART_DMAPause(&huart1);

		memset(NMEA_string_buffer, 0, SIZE_UINT8(DMA_RX_BUFFER_SIZE));

		uint8_t *current = (uint8_t *)DMA_RX_buffer + (DMA_RX_BUFFER_SIZE-1 - huart1.hdmarx->Instance->CNDTR);

		if(current > prev)
		{
			uint8_t size = current - prev + 1;
			memcpy((uint8_t *)NMEA_string_buffer, (void *)prev, SIZE_UINT8(size));
			//memset((void *)DMA_RX_buffer, 0, DMA_RX_BUFFER_SIZE);
		}
		else if (prev > current)
		{
			uint8_t endSlice = (DMA_RX_buffer + DMA_RX_BUFFER_SIZE) - prev;
			uint8_t startSlice = (current - DMA_RX_buffer +1 );
			memcpy((uint8_t *)NMEA_string_buffer, (void *)prev, SIZE_UINT8(endSlice));
			memcpy((uint8_t *)NMEA_string_buffer+SIZE_UINT8(endSlice), (void *)DMA_RX_buffer, SIZE_UINT8(startSlice));
		}

		prev = ++current;

		HAL_UART_DMAResume(&huart1);

		strings = NMEA_DecodeRawString((uint8_t *)&(NMEA_string_buffer[1]), (uint8_t *)NMEA_string_buffer+SIZE_UINT8(DMA_RX_BUFFER_SIZE-1));
		if(strings != NULL)
		{
			strings = NMEA_FilterQueueOnChecksum(strings);
			NMEA_UpdateInformationFromQueue(&info, strings);
			if(info.valid)
				HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		}
		else
		{
			uint32_t freeHeap = 0;
			freeHeap = xPortGetFreeHeapSize();
			if(freeHeap > 2048)
			{
				HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
				HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
			}
		}

		NMEA_FreeQueue(strings);

	}


}

/*******************************************************************************
 * Name:   static void thr_MS5607(void const *argument)
 * Inputs: void const *argument - nothing in this context
 * Return: None
 * Notes:  Handles the MS5607 communication
 ******************************************************************************/
static void thr_MS5607(void const *argument)
{
	(void) argument;

	MS5607_t ms;
	MS5607_PROM_t prom;
	MS5607_results_t res;

	ms.i2c = &hi2c1;
	ms.prom = &prom;
	ms.result = &res;

	MS5607_ReadPROM(&ms);

	for (;;)
	{
		osDelay(1000);
		MS5607_TakeReading(&ms);
	}
}

/*******************************************************************************
 * Name:   static void thr_camera(void const *argument)
 * Inputs: void const *argument - nothing in this context
 * Return: None
 * Notes:  Handles the camera
 ******************************************************************************/
static void thr_camera(void const *argument)
{
	(void) argument;

	osDelay(1000);

	__HAL_DCMI_DISABLE_IT(&hDcmiHandler, DCMI_IT_LINE | DCMI_IT_VSYNC);

	HAL_DCMI_Start_DMA(&hDcmiHandler, DCMI_MODE_CONTINUOUS,  (uint32_t)pBuffer , CAM_BUF_SIZE);

	for (;;)
	{
		osDelay(10000);
	}
}


