/*******************************************************************************
 * File:                    MS5607.h
 * Project:                 ULHAB MS5607
 * Date:                    04/02/2019
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             MS5607 Interfacing
 ******************************************************************************/

#ifndef MS5607_H_
#define MS5607_H_

/*******************************************************************************
 * Module includes
 ******************************************************************************/
#include "main.h"
#include "i2c.h"
#include "stdlib.h"
#include "math.h"

/*******************************************************************************
 * System Wide Module definitions
 ******************************************************************************/
#define MS5607_READ_ADR 0xEE
#define MS5607_WRITE_ADR 0xEF

#define MS5607_RESET 0x1E
#define MS56007_CONVERT_D1_256 0x40
#define MS56007_CONVERT_D1_512 0x42
#define MS56007_CONVERT_D1_1024 0x44
#define MS56007_CONVERT_D1_2048 0x46
#define MS56007_CONVERT_D1_4096 0x48
#define MS56007_CONVERT_D2_256 0x50
#define MS56007_CONVERT_D2_512 0x52
#define MS56007_CONVERT_D2_1024 0x54
#define MS56007_CONVERT_D2_2048 0x56
#define MS56007_CONVERT_D2_4096 0x58
#define MS5607_ADC_READ 0x00
#define MS5607_PROM_READ_BASE 0xA0

#define MS5607_PROM_ADR_MIN 0
#define MS5607_PROM_ADR_MAX 7

#define MS5607_TIMEOUT 1000
#define MS5607_CONVERSION_DELAY 10

#define MS5607_SUCCESS_MASK 0x00FFFFFF
#define MS5607_VALID_MASK 0x10000000

/*******************************************************************************
 * Module Specific Macros
 ******************************************************************************/
#define MS5607_PROM_READ_ADR(expr) (MS5607_PROM_READ_BASE | (((expr) << 1) & 0x0F))
#define MS5607_ValidateResult(expr) if((expr) & MS5607_VALID_MASK) return FALSE

/*******************************************************************************
 * Module specific types
 ******************************************************************************/

typedef enum {
	MS5607_TEMP,
	MS5607_PRESSURE
} MS5607_conversion_type_et;

/* Structure of the internal PROM */
typedef struct {
	uint16_t factory_data;
	uint16_t SENS;
	uint16_t OFF;
	uint16_t TCS;
	uint16_t TCO;
	uint16_t T_ref;
	uint16_t TEMPSENS;
	uint16_t serial_code;
} MS5607_PROM_t;

typedef struct {
	float temp;
	float pressure;
} MS5607_results_t;

typedef struct {
	I2C_HandleTypeDef *i2c;
	MS5607_PROM_t *prom;
	MS5607_results_t *result;
} MS5607_t;

/*******************************************************************************
 * Module Globals
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
bool_et MS5607_ReadPROM(MS5607_t *ms5607);
uint32_t MS5607_ReadTempPressure(MS5607_t *ms5607, MS5607_conversion_type_et type);
bool_et MS5607_TakeReading(MS5607_t *ms5607);

#endif /* MS5607_H_ */
