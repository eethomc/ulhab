/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		Main.c
 * Project:
 * Date:        04/02/2019 10:56:53
 * Author:      Thomas Cousins
 * Target:      STM32L4xx
 * Copyright:   Doayee 2019
 * Description: Main program body
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- System Overview ---------------------------------
 *
 * Description:
 *
 * Inputs:
 *
 * Outputs:
 *
 * Interrupts and Events:
 *
 * Main System Code:
 *
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- Relevant Files ----------------------------------
 *
 * main.h - header for this file
 *
 ******************************************************************************/
 

/*******************************************************************************
* Includes
******************************************************************************/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "i2c.h"
#include "gpio.h"
#include "MS5607.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/
 
/*******************************************************************************
 * System globals
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/
MS5607_t ms;
MS5607_PROM_t prom;
MS5607_results_t res;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void SystemClock_Config(void);

/*******************************************************************************
 * Name:   int main(void)
 * Inputs: None
 * Return: Integer - but not really
 * Notes:  Runs forever, main uController program
 ******************************************************************************/
int main(void)
{

  /* Reset of all peripherals, Initialises the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialise all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();

  ms.i2c = &hi2c1;
  ms.prom = &prom;
  ms.result = &res;

  MS5607_ReadPROM(&ms);

  /* Infinite loop */
  while (1)
  {
      HAL_Delay(1000);
	  MS5607_TakeReading(&ms);
	  HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
  }

}

/*******************************************************************************
 * Name:   void SystemClock_Config(void)                                      
 * Inputs: None                                                               
 * Return: None                                                               
 * Notes:  Configures the System Clock
 ******************************************************************************/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

}

/*******************************************************************************
 * Name:   void SpinWait(void)                                                  
 * Inputs: none                                                                 
 * Return: none                                                                 
 * Notes:  Not a place you want to end up                                       
 * Credit: Matt Creighton                                                       
 ******************************************************************************/
void SpinWait(void)
{
  /*shucks*/
  while(1);
}

/*******************************************************************************
 * Name:   void _Error_Handler(char * file, int line)
 * Inputs: char * file - String populated with filename
 *         int line - Line number of line in file
 * Return: None                                           
 * Notes:  Default error handler from STM32Cube, redirects to SpinWait because
 *         there is no debug output implemented.     
 ******************************************************************************/
void _Error_Handler(char * file, int line)
{
  SpinWait();
}

#ifdef  USE_FULL_ASSERT
/*******************************************************************************
 * Name:   void assert_failed(uint8_t* file, uint32_t line)
 * Inputs: char * file - See above
 *         int line - See above
 * Return: None
 * Notes:  Reports assert error. Not implemented (unless you choose to).
 ******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* Not implemented */
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/* EOF */
